

const express = require('express');
const OSB = require('../dist/api/OSB');
const OSBService = require('../dist/api/OSBService');
const Validator = require('../dist/lib/validator');

const osbPath = '/osb';

describe('OSB', function () {
  const sandbox = sinon.sandbox.create();
  let app, broker, service;
  let req, res;

  // setup express app and register broker
  before(() => {
    app = express();
  });

  beforeEach(function () {
    sandbox.stub(Validator.prototype, 'validate');

    broker = new OSB(app, {osbPath: osbPath});

    service = new OSBService({
      getServiceInfo: async function () {
        return {
            id: 'service-id'
        };
      },
      provisionServiceInstance: () => Promise.resolve(),
      updateServiceInstance: () => Promise.resolve(),
      deprovisionServiceInstance: () => Promise.resolve(),
      bindService: () => Promise.resolve(),
      unbindService: () => Promise.resolve(),
      pollLastOperation: () => Promise.resolve()
    });

    req = sandbox.spy();

    res = {type: function(){}, send: function () {}};

    sandbox.stub(res, 'type')
      .returns(res);
    sandbox.stub(res, 'send');
  });

  afterEach(function () {
    sandbox.restore();
  });

  describe('#registerService', function () {
    it('Fail when missing service', async function () {
      await broker.registerService().should.be.rejectedWith(Error, 'Service is required');

      sinon.assert.callCount(Validator.prototype.validate, 0);
    });

    it('Fail when do not implement required methods', async function () {
      service['getServiceInfo'] = undefined;
      service['provisionServiceInstance'] = undefined;

      await broker.registerService(service)
          .should.be.rejectedWith(Error, 'getServiceInfo,provisionServiceInstance,updateServiceInstance,' +
        'deprovisionServiceInstance,bindService,' +
        'unbindService,pollLastOperation need to be implemented. ' +
        'Missing methods: getServiceInfo,provisionServiceInstance');

      sinon.assert.callCount(Validator.prototype.validate, 0);
    });

    it('Fail when required method is not a function', async function () {
      service['getServiceInfo'] = 'getServiceInfo';

      await broker.registerService(service)
          .should.be.rejectedWith(Error, 'getServiceInfo,provisionServiceInstance,updateServiceInstance,' +
        'deprovisionServiceInstance,bindService,' +
        'unbindService,pollLastOperation need to be implemented. Missing methods: getServiceInfo');

      sinon.assert.callCount(Validator.prototype.validate, 0);
    });

    it('Fail with invalid service info', async function () {
      Validator.prototype.validate.returns(['serviceId is required']);

      await broker.registerService(service)
          .should.be.rejectedWith(Error, 'Invalid service info: serviceId is required');

      sinon.assert.calledOnce(Validator.prototype.validate);
    });

    it('Fail when service has been registered', async function () {
      Validator.prototype.validate.returns([]);

      await broker.registerService(service);

      await broker.registerService(service)
          .should.be.rejectedWith(Error, 'Service has already been registered with id service-id');

      sinon.assert.calledTwice(Validator.prototype.validate);
    });

    it('Pass', async function () {
      Validator.prototype.validate.returns([]);

      await broker.registerService(service);

      sinon.assert.calledOnce(Validator.prototype.validate);
    });
  });

  describe('#getCatalog', function () {
    it('Pass', async function () {
      Validator.prototype.validate.returns([]);

      await broker.registerService(service);
      await broker._getCatalog(req, res);

      sinon.assert.calledWith(res.type, 'json');
      sinon.assert.calledWith(res.send, '{"services":[{"id":"service-id"}]}');
    });
  });

  describe('#contextBuilder', function () {
    it('No context builder', async function () {
      Validator.prototype.validate.returns([]);

      sandbox.spy(service, 'getServiceInfo');

      await broker.registerService(service);
      await broker._getCatalog(req, res);

      sinon.assert.calledWith(service.getServiceInfo, {});
    });

    it('Set context builder', async function () {
      Validator.prototype.validate.returns([]);

      const context = {key: 'value'};

      broker = new OSB(app, {osbPath: osbPath, contextBuilder: function () {
        return context;
      }});

      sandbox.spy(service, 'getServiceInfo');

      await broker.registerService(service);
      await broker._getCatalog(req, res);

      sinon.assert.calledWith(service.getServiceInfo, context);
    });
  });
});
