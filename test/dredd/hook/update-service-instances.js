

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');
const chai = require('chai');
const should = chai.should();
const before = hooks.before;
const after = hooks.after;

const testData = require('../test-data');

// --- before hooks ---
before('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});


before('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

// --- after hooks - validation ---
after('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 200 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.EMPTY_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 202 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.OPERATION_IN_PROGRESS_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BAD_REQUEST));
});

after('service_instances > /v2/service_instances/{instance_id} > Updating a Service Instance > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify({description: 'broker only supports asynchronous provisioning for the requested plan'}));
});
