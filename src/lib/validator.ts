const Ajv = require('ajv');

class Validator {
  private name: string;
  private ajvValidator: any;

  /**
   * Create a validator for a single schema.
   *
   * @param {string} name - The name of the schema.
   * @param {Object} schema - The schema object.
   */
  constructor (name, schema) {
    this.name = name;
    this.ajvValidator = new Ajv({
      useDefaults: true,
      allErrors: true,
      jsonPointers: false,
      format: 'full',
      v5: true
    });
    this.ajvValidator.addSchema(schema, name);
  }

  /**
   * Using JSON schema to validate object
   *
   * @param {Object} object - The object to be validated against the configured schema.
   * @returns List of errors if any. Empty array indicates success.
   */
  validate (object): string[] {
    if (!object || typeof object !== 'object') {
      return ['object is empty or in an unexpected format'];
    }

    const result = this.ajvValidator.validate(this.name, object);
    if (result) {
      return [];
    } else {
      return Array.from(new Set(this.ajvValidator.errors.map(error => _prettyError(this.name, error))));
    }
  }
}

function _prettyError (name, err) {
  const pathWithoutDot = err.dataPath.substring(1);
  return `${pathWithoutDot.length === 0 ? name : pathWithoutDot} ${err.message}` +
         `${err.params.hasOwnProperty('additionalProperty') ? ': ' + err.params.additionalProperty : ''}`;
}

export = Validator;
