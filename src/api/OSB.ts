// eslint-disable-next-line no-unused-vars
import express = require('express');

import Validator = require('../lib/validator');

import ProvisionRequest = require('./model/request/ProvisionRequest');
import UpdateRequest = require('./model/request/UpdateRequest');
import DeprovisionRequest = require('./model/request/DeprovisionRequest');
import BindServiceRequest = require('./model/request/BindServiceRequest');
import UnbindServiceRequest = require('./model/request/UnbindServiceRequest');
import LastOperationRequest = require('./model/request/LastOperationRequest');
import BrokerError = require('./model/BrokerError');
// eslint-disable-next-line no-unused-vars
import OSBService = require('./OSBService');
// eslint-disable-next-line no-unused-vars
import Service = require('./model/Service');

const REQUIRED_METHODS = [
  'getServiceInfo',
  'provisionServiceInstance',
  'updateServiceInstance',
  'deprovisionServiceInstance',
  'bindService',
  'unbindService',
  'pollLastOperation'
];

const schemaMap = {
  'service': require('../schema/service.json'),
  'provision-request': require('../schema/provision-request.json'),
  'update-request': require('../schema/update-request.json'),
  'deprovision-request': require('../schema/deprovision-request.json'),
  'bind-request': require('../schema/bind-request.json'),
  'unbind-request': require('../schema/unbind-request.json'),
  'last-operation-request': require('../schema/last-operation-request.json')
};

type ContextBuilderType = (req: express.Request) => Promise<any>;

/**
 * Gets 'accepts_incomplete' from request params
 * @private
 */
function _getAcceptIncomplete (req): boolean {
  if (req.query && 'true' === req.query['accepts_incomplete']) {
    return true;
  }

  return false;
}

/**
 * Sends json response back to clients
 * @param res express app response object
 * @param data data returned from {@link OSBService} in format
 * {statusCode: 'http status code', data: 'actual data'}
 * @private
 */
function _returnJson (res, data): void {
  const statusCode = data.statusCode ? data.statusCode : '200';

  res.type('json')
    .status(statusCode)
    .send(JSON.stringify(data.data));
}

/**
 * Uses JSON schema to validate object
 *
 * @param name schema name
 * @param object object to be validated
 * @return List of errors (string) if any
 * @private
 */
function _validate (name, object): string[] {
  const validator = new Validator(name, schemaMap[name]);
  return validator.validate(object);
}

/**
 * Uses JSON schema to validate request.
 *
 * @param schemaName schema name
 * @param request the request to be validated
 * @param res express app response request
 * @return true if success
 * or false and send '400' response with detail errors to clients if fail
 * @private
 */
function _validateRequest (schemaName, request, res): boolean {
  const errors = _validate(schemaName, request);

  if (errors && errors.length) {
    res.type('json')
      .status(400)
      .send(JSON.stringify(new BrokerError({error: 'Request failed to validate', description: errors.join('\n')})));

    return false;
  }

  return true;
}

async function _buildContext (contextBuilder: ContextBuilderType, req: express.Request): Promise<any> {
  if (!contextBuilder) {
    return {};
  }

  return await contextBuilder(req);
}

/**
 * Broker class which allow clients to register OSB services
 */
class OSB {
  private services: {[key: string]: OSBService};
  private osbPath: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private app: express.Application;
  private contextBuilder: any;

  /**
   *
   * @param app 'express'
   * @param osbPath base path for OSB endpoints
   * @param contextBuilder function to build context for each request
   */
  public constructor(app: express.Application, {osbPath, contextBuilder}: {osbPath: string, contextBuilder: ContextBuilderType}) {
    this.services = {};
    this.osbPath = osbPath;
    this.app = app;
    this.contextBuilder = contextBuilder;
  }

  /**
   * Register an OSB service
   * @param service an instance of {@link OSBService}
   */
  public async registerService(service: OSBService): Promise<void> {
    if (!service) {
      throw new Error('Service is required');
    }

    const unimplementedMethods = REQUIRED_METHODS.filter((m): boolean => !(service[m] instanceof Function));
    if (unimplementedMethods.length) {
      throw new Error(`${REQUIRED_METHODS} need to be implemented. Missing methods: ${unimplementedMethods}`);
    }

    const serviceInfo = await service.getServiceInfo();

    const errors = _validate('service', serviceInfo);
    if (errors && errors.length) {
      throw new Error(`Invalid service info: ${errors}`);
    }

    const serviceId = serviceInfo.id;
    if (this.services[serviceId]) {
      throw new Error(`Service has already been registered with id ${serviceId}`);
    }

    this.services[serviceId] = service;
  }

  /**
   * Start broker, handle OSB requests
   */
  public start(): void {
    this.app.get(`${this.osbPath}/catalog`, this._getCatalog.bind(this));

    this.app.put(`${this.osbPath}/service_instances/:instanceId`, this._provisionServiceInstance.bind(this));

    this.app.patch(`${this.osbPath}/service_instances/:instanceId`, this._updateServiceInstance.bind(this));

    this.app.delete(`${this.osbPath}/service_instances/:instanceId`, this._deprovisionServiceInstance.bind(this));

    this.app.put(`${this.osbPath}/service_instances/:instanceId/service_bindings/:bindingId`,
      this._bindService.bind(this));

    this.app.delete(`${this.osbPath}/service_instances/:instanceId/service_bindings/:bindingId`,
      this._unbindService.bind(this));

    this.app.get(`${this.osbPath}/service_instances/:instanceId/last_operation`, this._pollLastOperation.bind(this));
  }

  /**
   * Handle get catalog request. Call to all registered services to get service info to build catalog data.
   * @private
   */
  private async _getCatalog (req, res) {
    const context = await _buildContext(this.contextBuilder, req);
    const services = await Promise.all(Object.values(this.services)
      .map(async (service): Promise<Service> => {
        const serviceInfo = await service.getServiceInfo(context);
        const errors = _validate('service', serviceInfo);
        if (errors && errors.length) {
          throw new Error(`Invalid service info: ${errors}`);
        }
        return serviceInfo;
      }));

    res.type('json')
      .send(JSON.stringify({services: services}));
  }

  /**
   * Handle provision service instance request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _provisionServiceInstance (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new ProvisionRequest(req.body);
    request.acceptsIncomplete = _getAcceptIncomplete(req);
    request.instanceId = req.params.instanceId;

    if (_validateRequest('provision-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).provisionServiceInstance(request, context));
    }
  }

  /**
   * Handle update provision service instance request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _updateServiceInstance (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new UpdateRequest(req.body);
    request.acceptsIncomplete = _getAcceptIncomplete(req);
    request.instanceId = req.params.instanceId;

    if (_validateRequest('update-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).updateServiceInstance(request, context));
    }

  }

  /**
   * Handle deprovision service instance request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _deprovisionServiceInstance (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new DeprovisionRequest(req.query);
    request.acceptsIncomplete = _getAcceptIncomplete(req);
    request.instanceId = req.params.instanceId;

    if (_validateRequest('deprovision-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).deprovisionServiceInstance(request, context));
    }
  }

  /**
   * Handle bind service request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _bindService (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new BindServiceRequest(req.body);
    request.instanceId = req.params.instanceId;
    request.bindingId = req.params.bindingId;

    if (_validateRequest('bind-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).bindService(request, context));
    }
  }

  /**
   * Handle unbind service request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _unbindService (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new UnbindServiceRequest(req.query);
    request.instanceId = req.params.instanceId;
    request.bindingId = req.params.bindingId;

    if (_validateRequest('unbind-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).unbindService(request, context));
    }
  }

  /**
   * Handle last operation request. Delegate to registered service with requested id to do the work
   * or throw Error if service with requested id has not been registered
   * @private
   */
  private async _pollLastOperation (req, res): Promise<void> {
    const context = await _buildContext(this.contextBuilder, req);
    const request = new LastOperationRequest(req.query);
    request.instanceId = req.params.instanceId;

    if (_validateRequest('last-operation-request', request, res)) {
      _returnJson(res, await this._getService(request.serviceId).pollLastOperation(request, context));
    }
  }

  /**
   * Get registered service with requested id or throw error if not found
   */
  _getService (serviceId) {
    if (!this.services[serviceId]) {
      throw new Error(`Service with id ${serviceId} has not been registered`);
    }

    return this.services[serviceId];
  }
}

export = OSB;
