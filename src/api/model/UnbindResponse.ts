/**
 * OSB Unbind Response
 */
class UnbindResponse {
    public operation: string;

    public constructor({operation}: {operation: string}) {
        this.operation = operation;
    }
}

export = UnbindResponse;
