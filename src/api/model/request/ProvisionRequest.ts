/**
 * OSB Provision Request
 */
class ProvisionRequest {
    public instanceId: string;
    public serviceId: string;
    public planId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public context: any;
    public organizationGuid: string;
    public spaceGuid: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public parameters: any;
    public acceptsIncomplete: boolean;

    public constructor({instance_id, service_id, plan_id, context,
                organization_guid, space_guid, parameters, accepts_incomplete}: {
        instance_id: string,
        service_id: string,
        plan_id: string,
        context: any,
        organization_guid: string,
        space_guid: string,
        parameters: any,
        accepts_incomplete: boolean
    }) {
        this.instanceId = instance_id;
        this.serviceId = service_id;
        this.planId = plan_id;
        this.context = context;
        this.organizationGuid = organization_guid;
        this.spaceGuid = space_guid;
        this.parameters = parameters;
        this.acceptsIncomplete = accepts_incomplete;
    }
}

export = ProvisionRequest;
