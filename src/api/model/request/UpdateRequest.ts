import PreviousValues = require('./PreviousValues');

/**
 * OSB Update Service Instance Request
 */
class UpdateRequest {
    public instanceId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public context: any;
    public serviceId: string;
    public planId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public parameters: any;
    public previousValues: PreviousValues;
    public acceptsIncomplete: boolean;

    public constructor({instance_id, context, service_id, plan_id,
                parameters, previous_values, accepts_incomplete}) {
        this.instanceId = instance_id;
        this.context = context;
        this.serviceId = service_id;
        this.planId = plan_id;
        this.parameters = parameters;
        if (previous_values) {
            this.previousValues = new PreviousValues(previous_values);
        }
        this.acceptsIncomplete = accepts_incomplete;
    }
}

export = UpdateRequest;
