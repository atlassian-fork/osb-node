/**
 * OSB Deprovision Request
 */
class DeprovisionRequest {
  public instanceId: string;
  public serviceId: string;
  public planId: string;
  public acceptsIncomplete: boolean;

  public constructor({instance_id, service_id, plan_id, accepts_incomplete}) {
    this.instanceId = instance_id;
    this.serviceId = service_id;
    this.planId = plan_id;
    this.acceptsIncomplete = accepts_incomplete;
  }
}

export = DeprovisionRequest;
