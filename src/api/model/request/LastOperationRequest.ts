/**
 * OSB Last Operation
 */
class LastOperationRequest {
  public instanceId: string;
  public serviceId: string;
  public planId: string;
  public operation: string;

  public constructor({instance_id, service_id, plan_id, operation}) {
    this.instanceId = instance_id;
    this.serviceId = service_id;
    this.planId = plan_id;
    this.operation = operation;
  }
}

export = LastOperationRequest;
