/**
 * OSB Service Metadata Object
 */
class ServiceMetadata {
  public displayName: string;
  public imageUrl: string;
  public longDescription: string;
  public providerDisplayName: string;
  public documentationUrl: string;
  public supportUrl: string;

  public constructor({displayName, imageUrl, longDescription, providerDisplayName, documentationUrl, supportUrl}) {
    this.displayName = displayName;
    this.imageUrl = imageUrl;
    this.longDescription = longDescription;
    this.providerDisplayName = providerDisplayName;
    this.documentationUrl = documentationUrl;
    this.supportUrl = supportUrl;
  }
}

export = ServiceMetadata;
