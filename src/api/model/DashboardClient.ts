/**
 * OSB Dashboard Client Object
 */
class DashboardClient {
  public id: string;
  public secret: string;
  public redirect_uri: string;

  public constructor({id, secret, redirect_uri}) {
    this.id = id;
    this.secret = secret;
    this.redirect_uri = redirect_uri;
  }
}

export = DashboardClient;
