/**
 * OSB Cost Object
 */
class Cost {
  public amount: {[currency: string]: number};
  public unit: string;

  public constructor({amount, unit}: {amount: {[currency: string]: number}, unit: string}) {
    this.amount = amount;
    this.unit = unit;
  }
}

export = Cost;
