/**
 * OSB Deprovision Response
 */
class DeprovisionResponse {
    public operation: string;

    public constructor({operation}: {operation: string}) {
        this.operation = operation;
    }
}

export = DeprovisionResponse;
