/**
 * OSB Last Operation Response
 */
class LastOperation {
  /*
  Valid values:
  "in progress"
  "succeeded"
  "failed"
   */
  public state: string;
  public description: string;

  public constructor({state, description}) {
    this.state = state;
    this.description = description;
  }
}

export = LastOperation;
