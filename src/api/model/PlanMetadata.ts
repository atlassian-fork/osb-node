/**
 * OSB Plan Metadata Object
 */
import Cost = require('./Cost');

class PlanMetadata {
  public bullets: string[];
  public costs: Cost[];
  public displayName: string;

  public constructor({bullets, costs, displayName}) {
    this.bullets = bullets;
    if (costs) {
      this.costs = costs.map(c => new Cost(c));
    }
    this.displayName = displayName;
  }
}

export = PlanMetadata;
