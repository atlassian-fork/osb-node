# atlassian / osb-node

A node library to implement an OSB broker which can register multiple OSB services

## Overview
- 
## OSB Classes
- `api/OSB.js`: broker class which allow clients to register OSB services. 
  + `app` - REQUIRED: express app
  + `osbPath` - REQUIRED: base path for OSB endpoints
  + `contextBuilder` - OPTIONAL: function to build context for each requests
- `api/OSBService`: represent an OSB service which handle OSB requests. Each `OSBService` needs to implement below functions to handle OSB requests for the service.
  + `getServiceInfo`: return service info
  + `provisionServiceInstance`: async function to provision service instance
  + `updateServiceInstance`: async function to update service instance
  + `deprovisionServiceInstance`: async function to deprovision service instance
  + `bindService`: async function to bind service
  + `unbindService`: async function to unbind service
  + `pollLastOperation`: async function to get last operation status
  + *Each function must return in format:* 

```
{ 
  statusCode: 'the-http-status-code',
  data: 'the response'
}
```

-  `api/model/*`: model classes represent OSB responses
-  `api/model/request/*`: model classes represent OSB request params


## Example usages
### Create a broker and register a service
```
const express = require('express');
const osbNode = require('osb-node');
const OSB = osbNode.OSB;
const OSBService = osbNode.OSBService;

const app = express();
const broker = new OSB(app, {osbPath: osbPath, contextBuilder: function(httpRequest){
	return {key: 'value};
});
broker.registerService(new OSBService({
  getServiceInfo,
  provisionServiceInstance,
  updateServiceInstance,
  deprovisionServiceInstance,
  bindService,
  unbindService,
  pollLastOperation
}));
broker.start();

```

### Handle OSB requests of a service
#### Get servie info
```
const Plan = osbOnode.modelPlan;
const Schemas = osbOnode.modelSchemas;
const ServiceBindingSchema = osbOnode.modelServiceBindingSchema;
const Service = osbOnode.modelService;
const ServiceInstanceSchema = osbOnode.modelServiceInstanceSchema;

function getServiceInfo(request, context) {
  return new Service({
    name: 'service-a',
    id: 'service-id',
    description: 'service A',
    tags: ['tag', 'osb'],
    requires: ['route_forwarding'],
    bindable: true,
    metadata: {
      displayName: 'Service 1',
      provider: {
        name: 'provider name'
      }
    },
    dashboard_client: new DashboardClient({
      id: '7545b455-9cdb-41d2-abf3-3a4bae383a43',
      secret: '277cabb0-XXXX-XXXX-XXXX-7822c0a90e5d',
      redirect_uri: 'http://localhost:1234'
    }),
    plan_updateable: true,
    plans: [new Plan({
      id: 'ab3eb1db-26d7-429a-850a-daeb667e0530',
      name: 'plan-1',
      description: 'description',
      metadata: {
        max_storage_tb: 5,
        costs: [{
          amount: {
            usd: 99.0
          },
          unit: 'MONTHLY'
        }, {
          amount: {
            usd: 0.99
          },
          unit: '1GB of messages over 20GB'
        }],
        bullets: [
          'Shared fake server',
          '5 TB storage',
          '40 concurrent connections'
        ]
      },
      schemas: new Schemas({
        service_instance: new ServiceInstanceSchema({
          create: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          }),
          update: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          })
        }),
        service_binding: new ServiceBindingSchema({
          create: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          })
        })
      })
    }, new Plan({
      id: '11f48b05-e3f3-4265-bdf7-6a0c58dbc9bf',
      name: 'plan-2',
      description: 'Shared fake Server, 5tb persistent disk, 40 max concurrent connections. 100 async',
      free: false,
      metadata: {
        max_storage_tb: 5,
        costs: [{
          amount: {
            usd: 199.0
          },
          unit: 'MONTHLY'
        }, {
          amount: {
            usd: 0.99
          },
          unit: '1GB of messages over 20GB'
        }],
        bullets: [
          '40 concurrent connections'
        ]
      }
    }))]
  });
}
```

#### Handle provision request
```
const ProvisionResponse = osbNode.model.ProvisionResponse;
function provisionServiceInstance(request, context) {
  return {
    statusCode: '200',
    data: new ProvisionResponse({dashboard_url: 'https://service-dashboard', operation: 'provision'});
  }
}
```

#### Last operation
```
const LastOperation = osbNode.model.LastOperation;
new LastOperation({state: 'in progress', description: 'provisioning instance'});
```

#### Broker error
```
const BrokerError = osbNode.model.BrokerError;
new BrokerError({error: 'invalid request', description: 'service id is required'});

```

***More details are in*** [test-app.js](./test/dredd/test-app.js)


## Code of Conduct

Please see [CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md)


## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md)


## License

We license this project under the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0),
as per our [LICENSE.txt](./LICENSE.txt)
